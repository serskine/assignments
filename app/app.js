'use strict';

console.log("HELLO WORLD FROM ANGuLAR!");


/**
 * Created by Owner on 6/7/2017.
 */


/**
 * Settings
 */
var DEFAULT_DISPLAY = true;
var DEFAULT_TYPE = 'text';
var DEFAULT_DATA = 'Undefined Data';
var DEFAULT_INDEX = 0;

// Used to exclude the Custom Content div for specific browser sizes
var EXCLUDE_CUSTOM_CONTENT = {
    minWidth: 768,
    maxWidth: 1280
};
var SERVER_HOST = 'http://localhost';
var SERVER_PORT = 3000;
var SERVER_URL = SERVER_HOST + ":" + SERVER_PORT + "/"; // This will allow us to query the server

/**
 * This defines the order of priority for each of the list element styles
 * @type {[*]}
 */
var LIST_STYLE_NONE = -1;
var LIST_STYLE_FIBBO = 0;
var LIST_STYLE_MIDDLE = 1;
var LIST_STYLE_LAST = 2;
var LIST_STYLE_FIRST = 3;

var listElementStyleOrder = ['fibbonacci', 'middle', 'last', 'first'];

/**
 * This function will ensure that a list element can only have one of the common list element styles
 * applies to it and that it matches the index provided. If the index is out of bounds all styles will be removed.
 * @param listElement
 * @param styleIndex
 */
var setListElementStyle = function(listElement, styleIndex) {
    var len = listElementStyleOrder.length;
    for(var i=0; i<len; i++) {
        if (i===styleIndex) {
            listElement.classList.add(listElementStyleOrder[i]);
        } else {
            listElement.classList.remove(listElementStyleOrder[i]);
        }
    }
}

/**
 * Used to return a string representing the style
 * @param styleIndex
 * @returns {*}
 */
var getStyleString = function(styleIndex) {
    var len = listElementStyleOrder.length;
    if (styleIndex < 0 || styleIndex >= len) {
        return 'none';
    } else {
        return listElementStyleOrder[styleIndex];
    }
}


/**
 * This funciton will be called when we fail to load the filename indicated by the LOAD_FILE variable
 * @param data
 */
var onLoadContentsFailure = function(error) {
    var message = "Failed to load data from " + SERVER_URL + ". " + error.text;
    console.log(message);
    alert(message);
};

/**
 * Called when we fail to save data
 * @param error
 */
var onSaveContentsFailure = function(error) {
    var message = "Failed to save data at " + SERVER_URL + ". " + error.text;
    console.log(message);
    alert(message);
}


/**
 * Internal variables
 */
var jsonListData = [];  // A list of json objects we want to display


/**
 * This function will be called when we successfully load the data file
 * @param jsonData
 */
var updateUiWithJsonData = function(jsonData) {
    console.log('JSON data loaded.');
    jsonListData = jsonData;
    updateUiListContents(jsonData);
    updateUiCustomContent(jsonData);
};

/**
 * This will return a formatted string that's pleasing to the eye when printer
 * @param jsonData
 */
var getJsonText = function (jsonData) {
    return JSON.stringify(jsonData, null, '  ');
}


/**
 * This will change the text on the custom content label
 * @param jsonData
 */
var updateUiCustomContent = function(jsonData) {
    var text = getJsonText(jsonData);
    $('#theContentRow').text(text);
}

/**
 * This will update the list contents. This may also filter out the results
 * @param jsonData
 */
var updateUiListContents = function(jsonData) {
    clearListContents();

    var theCheckBox = $('#theDisplayFilter');
    var checkValue = theCheckBox.prop('checked');
    var theList = $('#theItemList')[0]; // First entry. Should only be one with the same id anyways

    // This will add only the desired items we want to the visible list
    $(jsonData.content).each(function(index, value) {
        var listItem = createListItem(index, value);
        var addToUi = (!checkValue || listItem.display);

        // We only add the item to the ui if the display filter is off or the display property if true
        if (addToUi)
        {
            addItemToUiList(listItem, theList);
        }
    });

    // Because the contents have been updated, we will need to refresh the ui styles for all the list elements
    checkListStyles();

}

/**
 * This will add a new list element to the list
 * @param listItem is the listItem data we use to add to the list
 * @param theList is the list on the web page we want to add a new list element to
 */
var addItemToUiList = function(listItem, theList) {
    var node = document.createElement("li");
    var textNode = document.createTextNode(listItem.description);
    node.appendChild(textNode);
    theList.appendChild(node);
}

/**
 * This will remove all the list items from the displayed list
 */
var clearListContents = function() {
    $('#theItemList').empty();

    // Determine what style we should use for each index an apply it to the list element.
    $('#theItemList.children').each(function(index, value) {
        var listStyleIndex = getListItemStyleIndex(index, firstIndex, middleIndex, lastIndex);
        var styleDesc = getStyleString(listStyleIndex);
        setListElementStyle(value, listStyleIndex);
    })
}


/**
 * This will iterate over all the list elements and adjust their styles accordingly
 * @param useDisplayFilter
 */
var checkListStyles = function() {
    var theList = $('#theItemList')[0];

    var numItems = theList.childElementCount;
    var firstIndex = 0;
    var middleIndex = Math.floor((numItems-1)/2);
    var lastIndex = numItems-1;

    // Determine what style we should use for each index an apply it to the list element.
    $(theList.children).each(function(index, value) {
        var listStyleIndex = getListItemStyleIndex(index, firstIndex, middleIndex, lastIndex);
        var styleDesc = getStyleString(listStyleIndex);
        setListElementStyle(value, listStyleIndex);
    })
}

/**
 * This will determine which style index should be used for the given list index
 * @param index
 * @param firstIndex
 * @param middleIndex
 * @param lastIndex
 * @returns {*}
 */
var getListItemStyleIndex = function(index, firstIndex, middleIndex, lastIndex) {

    // check for the first color index
    if (index===firstIndex) {
        return LIST_STYLE_FIRST;
    }

    // Check for the last color index
    if (index===lastIndex) {
        return LIST_STYLE_LAST;
    }

    // Check for the middle index
    if (index===middleIndex) {
        return LIST_STYLE_MIDDLE;
    }

    // Check if the index is in the fibbonacci sequence.
    if (isFibbonacciNumber(index)) {
        return LIST_STYLE_FIBBO;
    }

    // Remove the list styles for the element
    return LIST_STYLE_NONE;
}

/**
 * This will determine if a value is a perfect square of some integer
 * @param number
 * @returns {boolean}
 */
var isPerfectSquare = function(number) {
    return (Math.sqrt(number) %1 === 0);
}

/**
 * This function may have problems as the upper bounds of the numbers is reached somewhere around the 70th
 * number in the fibbonacci sequence.
 * This will return true if the provided number is in a fibbonacci sequence.
 */
var isFibbonacciNumber = function(value) {
    var square = value*value;
    var check1 = 5 * square + 4;
    var check2 = 5 * square - 4;

    // If either one of the two values is a perfect square, then this is a fibbonacci number (check1 or check2)
    if (isPerfectSquare(check1) || isPerfectSquare(check2)) {
        return true;
    } else {
        return false;
    }
}


/**
 * this will return an element that makes sense
 * @param index is the index in the json. This can be undefined
 * @param item is json data. This will be parsed. If a property is not present it will be set in the new ListItem
 */
var createListItem = function(index, item) {
    var listItem = {
        display: DEFAULT_DISPLAY,
        type: DEFAULT_TYPE,
        data: DEFAULT_DATA,
        index: DEFAULT_INDEX,
        description: ""
    };

    var display = item.display;
    if (display===undefined) {
        display = DEFAULT_DISPLAY;
    }

    var type = item.type;
    if (type===undefined) {
        type = DEFAULT_TYPE;
    }

    var data = item.data;
    if (data===undefined) {
        type = DEFAULT_DATA;
    }

    listItem.data = data;
    listItem.display = display;
    listItem.type = type;
    listItem.index = index;

    // var description = "ListItem[" + index + "]: " + listItem.display;
    var description = getJsonText(data);
    listItem.description = description;

    return listItem;
}



/**
 * This method will be called whenever the display filter checkbox is updated
 */
var onCheckBoxChanged = function() {
    updateUiListContents(jsonListData);
}


/**
 * This will look at the UI and determine a new json element to add to our loaded list of items
 * @returns {{display: boolean, data: string, type: string}}
 */
var createJsonDataFromUi = function() {
    var text = $("#theTextInput").val();
    console.log(text);

    var jsonItem = {
        display: true,
        data: text,
        type: "text"
    };
    return jsonItem;
}

/**
 * This method is used to get the browser size
 */
var getBrowserSize = function() {
    var width, height;

    if (typeof window.innerWidth != 'undefined') {
        width = window.innerWidth;
        height = window.innerHeight;
    }
    else if (typeof document.documentElement != 'undefined'
        && typeof document.documentElement.clientWidth != 'undefined'
        && document.documentElement.clientWidth != 0
    ) {
        width = document.documentElement.clientWidth; //IE
        height = document.documentElement.clientHeight;
    } else {
        width = document.body.clientWidth; //IE
        height = document.body.clientHeight;
    }
    return {'width': width, 'height': height};
}

/**
 * This will check to see if the browser size width is in the exclusion range. And if so it will hide the content row
 */
var checkCustomContentVisibility = function() {
    var size = getBrowserSize();
    var width = parseInt(size.width);

    console.log("EXCLUSION = " + EXCLUDE_CUSTOM_CONTENT);
    console.log("SIZE      = " + size);
    console.log("WIDTH     = " + width);

    if  (   width >= EXCLUDE_CUSTOM_CONTENT.minWidth
        &&  width <= EXCLUDE_CUSTOM_CONTENT.maxWidth
    )
    {
        document.getElementById("theContentRow").style.display = "none";    // Do not display at all
    } else{
        document.getElementById("theContentRow").style.display = "block";   // Display as a block item
    }
}

/**
 * This methos will save all the items in the JSON list {@param jsonListData}
 */
var requestSaveItemsJson = function() {
    console.log("Request saving the json data from the server!!!");

    var bodyText = getJsonText(jsonListData);    // Enclose in a body tag
    // console.log("bodyText = " + bodyText);
    var serverUrl = SERVER_URL + "save";

    var data = {
        text: bodyText
    };

    console.log(" Url: " + serverUrl);
    // angular call to http post

    $.ajax({
        url: serverUrl,
        data: data,
        dataType: 'json',
        type: 'post',
        cache: false,
        success: updateUiWithJsonData,
        error: onLoadContentsFailure
    });

}


/**
 * This will load all the json data from the file and update the ui accordingly.
 * It will get the data by sending a request to the NodeJs server and then receiving the response.
 */
var requestJsonData = function() {
    var serverUrl = SERVER_URL + "load";
    console.log("Request loading the json data from the server!!!");
    console.log(" Url: " + serverUrl);
    $.ajax({
        url: serverUrl,
        dataType: 'json',
        type: 'get',
        cache: false,
        success: updateUiWithJsonData,
        error: onLoadContentsFailure
    });
}

/**
 * Called when the data has been loaded
 * @param data
 */
function onLoadData(data)
{
    console.log(data);
}

/**
 * This will set up the handler on the add button
 * If an error occurs while attempting to parse the input then an alert will be displayed and messages will be outputted to the console.
 */
var initAddButton = function() {
    $("#theAddbutton").click(function () {
        try {
            var jsonItem = createJsonDataFromUi();
            jsonListData.content.push(jsonItem);
            updateUiWithJsonData(jsonListData); // Because the underlying list of elements has changed, we need to update our ui
        } catch (error) {
            var message = "Failed to add an element to the list! ";
            console.log(message);
            console.log(error);
            alert(message + error);
        }
    });
}

/**
 * This will define the handler for the save button
 */
var initSaveButton = function() {
    $("#theSaveButton").click(function() {
        try {
            requestSaveItemsJson();
        } catch (error) {
            var message = "Failed to save the JSON. ";
            console.log(message);
            console.log(error);
            alert(message + error);
        }
    })
}

/**
 * This will set things up to listen to the window resizing
 */
var initResizeHandler = function() {
    // Add the window resize listener
    window.onresize = function(event) {
        checkCustomContentVisibility(); // We don't need the event
    };
}

/**
 * This will set up the handler for the checkbox
 */
var initCheckBox = function() {
    document.addEventListener('DOMContentLoaded', function () {
        document.querySelector('#theDisplayFilter').addEventListener('change', onCheckBoxChanged);
    });
}


/**
 * This method is called when the document is ready and has been loaded into the browser
 */
var onDocumentReady = function() {
    console.log("*****  onDocumentReady() *****");

    initResizeHandler();
    initAddButton();
    initSaveButton();
    initCheckBox();

    requestJsonData();
}


/**
 * EFFECTIVE ENTRY POINT!!!
 */

// start the program
$(document).ready(onDocumentReady());




