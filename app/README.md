**<h1>The Money Finder Assignment</h1>**

<h3>**Running the app**</h3> 

To run the app from the command line simply execute ```node index.html``` while in the 
app directory to get the server up and running. Then open a web browser on at ```http://localhost:3000``` to open up the
web page. It will get this from the server that is running.

Using this app is easy. As you will notice the layout matches the wireframe provided in the assignment. There are two
areas for display.

* <h3>**The External link**</h3> - This link is displayed on the center of the webpage near the bottom. Clicking this
 will take you to an external page that should display these instructions.

* **<h3>The Left Pane</h3>** - displays all the elements found at the root in the _content_ property of the 
 loaded json object. Each list element will display the data element as a formatted JSON
 object. Cells will have their color determine by the following priority sequence (highest to lowest).
 When we are referring to indicies such as _first, middle and last_, we are referring to those displayed on the UI.
 This fact becomes important when we refer to the _display_ property later.
    * If an element is the first element, then it's background will be red.
    * If an element is the last element, then it's background will be green.
    * If an element is at the middle index (rounded down) then it's background will be blue.
    * Finally if an element is at an index (starting at 0) that is a fibbonacci sequence the it's
      background will display as gold.
      
     
* <h3>**The Right Pane**</h3>
    This pane will contain all the controls needed to modify the loaded list of items as well as save and view content.
    
    * **Add Element Row** - A row to add a new element to the JSON array. A new object will be created and
    added to the json array. The _data_ property will match the text in the input box. Nothing will be added to the
    array until the add button has been pressed. The object created will follow the following structure.
        ```
        item {
            display:     [true|false],     // User values will always be set to 'true'
            type:        [text|list]       // User input will always be set to 'text'
            data:        value,            // Matches the text provided by the text area
        };
        ```
        Note that when parsing the json array elements, some or none of those value may be defined.
        If the property is not present then we assume default properties for them as specified below.
        ```
            DEFAULT_DISPLAY = true;
            DEFAULT_TYPE    = 'text';
            DEFAULT_DATA    = 'Undefined Data';
        ```
    * **Save Button** - This will cause the client to send the local copy of the list to the server with the intent
    to overwrite any values in the save file _save_content.json_. Because this file is not loaded whenever we reload
    the page, the local copy of the list items will resotred to match the contents of _content.json_. This is by design
    as it was explicitly stated in the assignment instructions.
    
    * **Display Filters Row** - This checkbox will determine which elements will be displayed on the left pane.
        * _checked_ - Only items with the display property set to true will be visible. 
        * _unchecked_ - All items will be displayed in the list.
         
    * **Custom Content** - This area is meant to display the entire json array object as a nicely formatted list.
    It is not meant for editing. This area will only be visible on screen sizes 768px and 1280px. It will always be
    visible if the screen size is less than 768px. The directions were not clear as to which dimension this referred
    to so I assumed it meant the horizontal dimension
    
**<h3>A word about design</h3>**
<p> As I progressed through the assignment, I realised there were things I should have done differently. This assignment
was a learning experience for me as I haven't developed in javascript for quite some time and needed a bit of time to get
used to the tools. I would normally include tests. I would also make certain the model was implemented in a more robust
way on the server using data access object (Dao's) on the client side to send the appropriate requests, better implementing
MVC design pattern. Finally I would have spent a lot of time making a fancy other link. Disclaimers aside, I did get 
the app to work and I'm glad I had this assignment. I do hope the app meet's your specifications and appreciate any
feedbacl you wish to provide.


   Regards

   Stuart Marr Erskine
   