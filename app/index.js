/***********************************************
 * The purpose of this file is to load up a Node.js server
 * that will respond to requests and send back the json data
 * in the form of a string.
 */

console.log("HELLO WORLD! from index.js");

var LOAD_FILE = 'content.json';
var SAVE_FILE = 'save_content.json';

var RESPONSE_CODE = {
    ok: 200,
    error: 500,
    bad_request: 400
};

/**
 * This method will start a new server for NODE.js
 */
var http = require('http');
var local_host = "http://localhost";
var server_port = 3000;
var server_url = local_host + ":" + server_port + "/";
var express = require('express');
var app=express();

app.use(express.static('.'));
app.use(express.static('website'));


// Set up the body parser
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded(
    { extended: false}
));
app.use(bodyParser.json());

var startServer = function() {
    console.log("STARTING SERVER");
    //http.createServer(onServerRequest).listen(server_port);
}



/**
 * This will return a formatted string that's pleasing to the eye when printer
 * @param jsonData
 */
var getJsonText = function (jsonData) {
    return JSON.stringify(jsonData, null, '  ');
}

/**
 * We have received a request to start a server
 * @param request
 * @param response
 */
var onServerRequest = function(request, response) {
    var message = 'Request received on port ' + server_port + ".\n";
    var jsonData = require('./content.json');
    var jsonText = getJsonText(jsonData);

    console.log(message);

    var method = request.method.toUpperCase();
    console.log("method = " + method);

    // Determine what to do with the request
    switch(method) {
        case 'GET':
            onLoadDataRequest(request, response);
            break;
        case 'POST':
            onSaveDataRequest(request, response);
            break;
        default:
            onUnknownRequest(request, response);
            break;
    }
}

/**
 * This method is called when we don't know what to do with the request
 * @param request
 * @param response
 */
var onUnknownRequest = function(request, response) {
    var message = "We recieved an unknown request type.";
    console.log("UNKNOWN REQUEST DETECTED!");

    // Send the success message back
    response.writeHead(
        RESPONSE_CODE.bad_request,    // Code ok
        {
            'Content-Type': 'text/json',
            'Access-Control-Allow-Origin': 'http://localhost:63342'
        }
    );
    response.write(message);
    response.end();


}

var logRequest = function(request) {

    console.log("------------------------------");

    // console.log(request.headers);
    console.log(text);
    // console.log(request.method);
    // console.log(request.body);
    // console.log(request.message);
    console.log("------------------------------");
}

/**
 * TODO: REMOVE
 * This method is called when we receive a save data request (PUT)
 * @param request
 * @param response
 */
var onSaveDataRequest = function(request, response) {
    console.log("SAVE REQUEST RECEIVED!");

    logRequest(request);
    var bodyText = request.body;


    // console.log("<body>");
    // console.log(bodyText);
    // console.log("</body>");
    //
    // try {
    //
    //     console.log("bodyText = " + bodyText);
    //     var fs = require('fs'); // get the filesystem
    //
    //     console.log("*** SAVING DATA ***");
    //     console.log(bodyText);
    //
    //     fs.writeFile (
    //         "save_content.json",
    //         bodyText,
    //         null
    //     );
    //
    //     // Send the success message back
    //     response.writeHead(
    //         RESPONSE_CODE.ok,    // Code ok
    //         {
    //             'Content-Type': 'text/utf-8',
    //             'Access-Control-Allow-Origin': 'http://localhost:63342'
    //         }
    //     );
    //     response.write(bodyText);
    //     response.end();
    //
    // } catch (error) {
    //
    //     console.log("Error is " + error);
    //
    //     // Send the success message back
    //     response.writeHead(
    //         RESPONSE_CODE.error,    // Code ok
    //         {
    //             'Content-Type': 'text/json',
    //             'Access-Control-Allow-Origin': 'http://localhost:63342'
    //         }
    //     );
    //     response.write("Failed to save the contents on the server!");
    //     response.end();
    // }

}

/**
 * TODO: REMOVE
 * This method will be called when we receive a get request for the json data (GET)
 * @param request
 * @param response
 */
var onLoadDataRequest = function(request, response) {
    try {
        var jsonData = require('./content.json');
        var jsonText = getJsonText(jsonData);

        console.log(jsonData);
        // Access-Control-Allow-Origin:
        // Create the response
        response.writeHead(
            RESPONSE_CODE.ok,
            {   // Code Ok
                'Content-Type': 'text/json',
                'Access-Control-Allow-Origin': 'http://localhost:63342'
            }
        );
        response.write(jsonText);
        response.end();
    } catch (error) {
        var message = "There was a server error attempting to load the data!";

        // Access-Control-Allow-Origin:
        // Create the response
        response.writeHead(
            RESPONSE_CODE.error,    // Code: Not implemented
            {
                'Content-Type': 'text/json',
                'Access-Control-Allow-Origin': 'http://localhost:63342'
            }
        );
        response.write(message);
        response.end();
    }
}


/**
 *  ENTRY POINT!!
 */

var listening = function() {
    console.log("Listening......");
}
var server = app.listen(server_port, startServer);   // Listening on local host
// startServer();
app.get('/', loadWebsite);
app.get('/load/', loadData);
app.post('/save/', saveData);

/**
 * This will simply serve up the main webpage
 */
function loadWebsite(request, response) {
    console.log("LOADING WEBSITE..... index.html");
    try {
        console.log("SUCCESS????");
    } catch (error) {
        console.log(error);
    }
}

/**
 * This will save all the data provided to it from the client
 * @param request
 * @param response
 */
function saveData(request, response) {
    console.log("SAVING DATA.....");
    try {
        var body = request.body;
        var bodyAsText = body.text;

        console.log("*** SAVING DATA ***");

        var fs = require('fs'); // get the filesystem
        fs.writeFile (
            "save_content.json",
            bodyAsText,
            new function () {
                console.log("Saved data!");
            }
        );

        // Send the success message back
        response.writeHead(
            RESPONSE_CODE.ok,    // Code ok
            {
                'Content-Type': 'text/utf-8',
                'Access-Control-Allow-Origin': 'http://localhost:63342'
            }
        );
        response.write("Successfully saved the data!");
        response.end();

    } catch (error) {
        console.log(error);

        // Send the success message back
        response.writeHead(
            RESPONSE_CODE.error,    // Code ok
            {
                'Content-Type': 'text/json',
                'Access-Control-Allow-Origin': 'http://localhost:63342'
            }
        );
        response.write("Failed to save the contents on the server!");
        response.write(error);
        response.end();
    }
}

/**
 * This will respond to load data requests
 * @param request
 * @param response
 */
function loadData(request, response) {
    console.log("LOADING DATA.....");
    try {
        var jsonData = require('./content.json');
        var jsonText = getJsonText(jsonData);
        response.writeHead(
            RESPONSE_CODE.ok,
            {   // Code Ok
                'Content-Type': 'text/json',
                'Access-Control-Allow-Origin': 'http://localhost:63342'
            }
        );
        response.write(jsonText);
        response.end();
    } catch (error) {
        var message = "There was a server error attempting to load the data!<br/>";
        response.writeHead(
            RESPONSE_CODE.error,
            {   // Code Ok
                'Content-Type': 'text/json',
                'Access-Control-Allow-Origin': 'http://localhost:63342'
            }
        );
        response.write(message);
        response.write(error);
        response.end();
    }
}





